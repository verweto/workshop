<?php

namespace WorkshopTest\Acceptance;

use PHPUnit_Extensions_Selenium2TestCase;

/**
 * Class GreetTest
 *
 * @group acceptance
 * @package WorkshopTest\Acceptance
 */
class GreetTest extends PHPUnit_Extensions_Selenium2TestCase
{
    protected function setUp()
    {
        $this->setHost('127.0.0.1');
        $this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://127.0.0.1:9000/');
    }

    /**
     * @test
     */
    public function it_should_display_the_visitors_name()
    {
        $this->url('/hello/Toon');

        $body = $this->byCssSelector('body');
        $this->assertEquals('Hello Toon', $body->text());
    }
}
