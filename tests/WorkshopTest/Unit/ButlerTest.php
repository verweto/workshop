<?php

namespace WorkshopTest\Unit;

use PHPUnit_Framework_TestCase;
use Workshop\Butler;

/**
 * Class ButlerTest
 *
 * @group integration
 * @package WorkshopTest\Unit
 */
class ButlerTest extends PHPUnit_Framework_TestCase
{


    /**
     * @test
     */
    public function it_should_greet_people()
    {
        $butler = new Butler();
        $this->assertEquals('Hello Toon', $butler->greet('Toon'));
    }

}
