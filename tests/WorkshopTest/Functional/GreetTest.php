<?php

namespace WorkshopTest\Functional;

use Silex\WebTestCase;

/**
 * Class GreetTest
 *
 * @group integration
 * @package WorkshopTest\Functional
 */
class GreetTest extends WebTestCase
{

    /**
     * @return \Symfony\Component\HttpKernel\HttpKernel
     */
    public function createApplication()
    {
        $app = require 'src/app.php';
        $app['debug'] = true;
        $app['exception_handler']->disable();
        return $app;
    }

    /**
     * @test
     */
    public function it_should_greet_any_visitor()
    {
        $client = $this->createClient();
        $client->request('GET', '/hello/Toon');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('Hello Toon', $client->getResponse()->getContent());
        $this->assertEquals('text/html; charset=UTF-8', $client->getResponse()->headers->get('Content-Type'));
    }

}
