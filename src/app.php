<?php

use Silex\Application;
use Workshop\Butler;

require __DIR__.'/../vendor/autoload.php';
$app = new Application();

// Index page
$app->get('/', function() use ($app) {
    return $app->redirect('/hello/Toon');
});

// Greeter
$app->get('/hello/{name}', function($name) use ($app) {
    $butler = new Butler();
    return $butler->greet($name);
});

return $app;