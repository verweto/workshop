<?php

namespace Workshop;

/**
 * Class Butler
 *
 * @package Workshop
 */
class Butler
{

    /**
     * Greets everybody!
     *
     * @param string $name
     *
     * @return string
     */
    public function greet($name)
    {
        return sprintf('Hello %s', $name);
    }
}
