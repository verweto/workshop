#!/bin/sh

cd `dirname $0`

if [ ! -f ./selenium-server-standalone-2.44.0.jar ]; then
    wget http://selenium-release.storage.googleapis.com/2.44/selenium-server-standalone-2.44.0.jar
fi;

java -jar ./selenium-server-standalone-2.44.0.jar