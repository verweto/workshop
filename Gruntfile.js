/*global module:false*/
module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    watch: {
      sourceFiles: {
        files: ['src/**/*.php', 'tests/**/*.php'],
        tasks: ['exec:lint', 'exec:phpcs', 'exec:phpunitIntegration']
      }
    },

    exec: {
      lint: {
        cmd: 'ant lint'
      },
      phpcs: {
        cmd: 'ant phpcs'
      },
      phpunitIntegration: {
        cmd: 'ant phpunit-integration'
      },
      phpunitAcceptance: {
        cmd: 'ant phpunit-acceptance'
      }
    }
  });

  // Default task.
  grunt.registerTask('default', ['watch']);

};
